#Comparison script
import os
import requests

input_hashes = open("small_list.txt","r")
api_output = open("api_output.txt","r+")
output_compromise = open("compromised_passwords.txt","w")


for h in input_hashes.readlines():
    h = h.strip().upper()
    resp = requests.get("https://api.pwnedpasswords.com/range/{0}".format(h[:5]))
    lines = resp.text.strip().split("\n")
    for line in lines:
        (hash_ending, count) = line.split(":")
        if h.endswith(hash_ending):
            print("Hash Compromised! {0} has {1} hits".format(h, count))
            api_output.write(f"Hash Compromised! {h} has {count} hits\n")
