#Script to run SHA-1 hash against passwords in a file and output

import hashlib

input_passwords = open("password_list.txt","r")
outputhash = open("hash_list.txt","w")

for line in input_passwords:
    eachpwd = line.strip()

    sha_1 = hashlib.sha1()
    sha_1.update(eachpwd.encode('utf-8'))
    outputhash.write(sha_1.hexdigest())
    outputhash.write("\n")
