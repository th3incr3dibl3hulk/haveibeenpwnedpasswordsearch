# HaveIBeenPwnedPasswordSearch

This repo contains scripts to ingest a txt list of passwords and run them against the public HaveIBeenPwned API to discover compromises: https://haveibeenpwned.com/API/v3